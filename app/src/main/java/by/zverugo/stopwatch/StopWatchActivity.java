package by.zverugo.stopwatch;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

public class StopWatchActivity extends Activity {

    private Timer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stop_watch);

        timer = savedInstanceState == null
                ? new Timer()
                : (Timer) savedInstanceState.getSerializable("timer");

        requireNonNull(timer);
        timer.runTimer((TextView) findViewById(R.id.time_view));
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putSerializable("timer", timer);
    }

    public void onClickStart(View view) {
        timer.start();
    }

    public void onClickStop(View view) {
        timer.stop();
    }

    public void onClickReset(View view) {
        timer.reset();
    }
}
