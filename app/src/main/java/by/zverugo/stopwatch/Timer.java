package by.zverugo.stopwatch;

import android.os.Handler;
import android.widget.TextView;

import java.io.Serializable;
import java.util.Locale;

public class Timer implements Serializable {
    private long seconds;
    private boolean running;

    public void start() {
        running = true;
    }

    public void stop() {
        running = false;
    }

    public void reset() {
        running = false;
        seconds = 0;
    }

    public void runTimer(final TextView timeView) {
        final Handler handler = new Handler();
        Runnable thread = new Runnable() {
            @Override
            public void run() {
                long hours = seconds/3600;
                long minutes = (seconds%3600)/60;
                long secs = seconds%60;
                String time = String.format(Locale.getDefault(),
                        "%d:%02d:%02d", hours, minutes, secs);
                timeView.setText(time);
                if (running) {
                    seconds++;
                }

                handler.postDelayed(this, 1000);
            }
        };

        handler.post(thread);
    }

    public long getSeconds() {
        return seconds;
    }

    public boolean isRunning() {
        return running;
    }
}
